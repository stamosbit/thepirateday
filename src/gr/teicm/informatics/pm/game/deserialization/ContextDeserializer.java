/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.deserialization;

import gr.teicm.informatics.pm.game.controller.Context;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import java.awt.Dimension;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * This class of objects can be used to deserialize <code>Context</code>
 * objects.
 * 
 * @author Stamos
 */
public final class ContextDeserializer implements IDeserializer<Context> {
    
    private static final String newLine = "\n";
    
    private static final String userPathName =
            System.getProperty("user.home");
    
    private static final String loadPathName = 
            "/teicm/The Pirate Day/save";
    
    private static final String loadFileName =
            "/context.ser";
    
    private final String absoluteLoadPathName;
    
    private Context context;
    
    public ContextDeserializer() {
        absoluteLoadPathName = userPathName + loadPathName + loadFileName;
        
        context = null;
    }
    
    @Override
    public void deserialize() {
        try {
            FileInputStream fileIn = new FileInputStream(absoluteLoadPathName);
            
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);
            
            context = (Context) objectIn.readObject();
            
            objectIn.close();
            
            fileIn.close();
            
            showSuccessDialog("Game successfully loaded!");
        } catch (IOException | ClassNotFoundException ex) {
            StringBuilder stringBuilder = new StringBuilder("Error: ");
            
            stringBuilder.append(ex.getMessage());
            
            stringBuilder.append(newLine);
            
            for (StackTraceElement element : ex.getStackTrace()) {
                stringBuilder.append(element.toString());
                
                stringBuilder.append(newLine);
            }
            
            showErrorDialog(stringBuilder.toString());
        }
    }
    
    @Override
    public Context getDeserialized() {
        return context;
    }
    
    private void showSuccessDialog(String infoMessage) {
        showDialog(infoMessage, "Success!", JOptionPane.INFORMATION_MESSAGE);
    }
    
    private void showErrorDialog(String errorMessage) {
        JTextArea textArea = new JTextArea(errorMessage);
        
        JScrollPane scrollPane = new JScrollPane(textArea) {
            @Override
            public Dimension getPreferredSize() {
                return new Dimension(480, 320);
            }
        };
        
        showDialog(scrollPane, "Error...", JOptionPane.ERROR_MESSAGE);
    }
    
    private void showDialog(
            Object statusMessage, String titleBar, int dialogType
    ) {
        JOptionPane.showMessageDialog(
                null, statusMessage, titleBar, dialogType
        );
    }
    
}
