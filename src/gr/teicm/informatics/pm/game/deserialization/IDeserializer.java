/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.deserialization;

/**
 * This interface is to be implemented by classes that will be used for
 * deserializing a specific object. The object which will be returned is from
 * the class replacing the parameter(T).
 * 
 * @author Stamos
 * @param <T>
 *        the class from which the deserialized object belongs.
 */
public interface IDeserializer<T> {
    
    void deserialize();
    
    T getDeserialized();
    
}
