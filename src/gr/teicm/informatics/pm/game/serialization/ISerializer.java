/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.serialization;

/**
 * This interface is to be implemented by classes that will be used for
 * serializing a specific object.
 * 
 * @author Stamos
 */
public interface ISerializer {
    
    void serialize();
    
}
