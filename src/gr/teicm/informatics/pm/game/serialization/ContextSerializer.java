/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.serialization;

import gr.teicm.informatics.pm.game.controller.Context;

import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.FileOutputStream;

import java.awt.Dimension;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * This class of objects can be used to serialize <code>Context</code> objects.
 * 
 * @author Stamos
 */
public final class ContextSerializer implements ISerializer {
    
    private static final String newLine = "\n";
    
    private static final String userPathName =
            System.getProperty("user.home");
    
    private static final String savePathName = 
            "/TEICM/The Pirate Day/save";
    
    private static final String saveFileName =
            "/context.ser";
    
    private final String absoluteSavePathName;
    
    private final Context context;
    
    public ContextSerializer(Context context) {
        absoluteSavePathName = userPathName + savePathName + saveFileName;
        
        this.context = context;
    }
    
    /**
     * Serializes the <code>Context</code> object to the disk.
     */
    @Override
    public void serialize() {
        try {
            File file = new File(userPathName + savePathName);
            
            file.mkdirs();
            
            FileOutputStream fileOut =
                    new FileOutputStream(absoluteSavePathName);
            
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            
            objectOut.writeObject(context);
            
            objectOut.close();
            
            fileOut.close();
            
            showSuccessDialog("Game successfully saved!");
        } catch (IOException ex) {
            StringBuilder stringBuilder = new StringBuilder(
                    "An error occured while loading: "
            );
            
            stringBuilder.append(ex.getMessage());
            
            stringBuilder.append(newLine);
            
            for (StackTraceElement element : ex.getStackTrace()) {
                stringBuilder.append(element.toString());
                
                stringBuilder.append(newLine);
            }
            
            showErrorDialog(stringBuilder.toString());
        }
    }
    
    private void showSuccessDialog(String infoMessage) {
        showDialog(
                infoMessage, "Success!", JOptionPane.INFORMATION_MESSAGE
        );
    }
    
    private void showErrorDialog(String errorMessage) {
        JTextArea textArea = new JTextArea(errorMessage);
        
        JScrollPane scrollPane = new JScrollPane(textArea) {
            @Override
            public Dimension getPreferredSize() {
                return new Dimension(480, 320);
            }
        };
        
        showDialog(
                scrollPane, "Error...", JOptionPane.ERROR_MESSAGE
        );
    }
    
    private void showDialog(
            Object statusMessage, String titleBar, int dialogType
    ) {
        JOptionPane.showMessageDialog(
                null, statusMessage, titleBar, dialogType
        );
    }
    
}
