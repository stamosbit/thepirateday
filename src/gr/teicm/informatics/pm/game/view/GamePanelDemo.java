/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.view;

import gr.teicm.informatics.pm.game.AbstractGame;
import gr.teicm.informatics.pm.game.TextBasedAdventureGame;
import gr.teicm.informatics.pm.game.command.AbstractCommand;
import gr.teicm.informatics.pm.game.util.IParser;
import gr.teicm.informatics.pm.game.util.ScannerParser;

import java.util.Scanner;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

/**
 * This class contains the <code>main</code> method. Its purpose is to organize
 * the GUI and present it accodringly to the user. It will provide the user the
 * ability to give input to the application and control the game's context.
 * 
 * @author Stamos
 */
public final class GamePanelDemo extends JPanel implements ActionListener {
    
    private static final String newLine = "\n";
    
    private static final String gameName = "The Pirate Day";
    
    private static final JFrame gameFrame = new JFrame(gameName);
    
    private static IParser<Scanner,AbstractCommand> parser;
    
    private static AbstractCommand command;
    
    private static AbstractGame game;
    
    private JTextField textField;
    
    private JTextArea textArea;
    
    private String inputText;
    
    private String gameOutcome;
    
    /**
     * The client's method.
     * 
     * @param args
     *        the command line arguments
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                parser = new ScannerParser();
                game = new TextBasedAdventureGame();
                initGameFrame();
            }
        });
    }
    
    /**
     * Initializes the GUI.
     */
    private static void initGameFrame() {
        gameFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gameFrame.setVisible(true);
        gameFrame.add(new GamePanelDemo());
        gameFrame.pack();
    }
    
    /**
     * Initializes a newly created <code>GamePanelDemo</code> object.
     */
    private GamePanelDemo() {
        super(new GridBagLayout());
        
        prepareSecondaryComponents();
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        inputText = textField.getText().trim();
        
        try {
            parseNewInput(inputText);
            
            assembleAction();
            
            game.play();
            
            gameOutcome = game.getOutcome();
            
            textArea.append(
                inputText + newLine +
                gameOutcome + newLine +
                newLine
            );
        } catch (IllegalArgumentException ex) {
            String message = ex.getMessage();
            
            textArea.append(
                inputText + newLine +
                message + newLine +
                newLine
            );
        }
        
        textField.setText("");

        // Make sure the new text is visible, even if there
        // was a selection in the text area.
        textArea.setCaretPosition(textArea.getDocument().getLength());
        
        game.resetOutcome();
    }
    
    /**
     * Initializes all apropiate secondary components and adds them to this
     * <code>JPanel</code> object.
     */
    private void prepareSecondaryComponents() {
        textField = new JTextField(20);
        textField.addActionListener(this);
        
        textArea = new JTextArea(5, 20);
        textArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(textArea);
        
        GridBagConstraints c = new GridBagConstraints();
        
        c.insets = new Insets(10, 10, 10, 10);
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.fill = GridBagConstraints.HORIZONTAL;
        add(textField, c);
        
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1.0;
        c.weighty = 1.0;        
        add(scrollPane, c);
    }
    
    /**
     * Creates an executable command from the <code>trimmedText</code> if it
     * matches to a valid command.
     * 
     * @param trimmedText
     */
    private static void parseNewInput(String trimmedText) {
        Scanner scanner = new Scanner(trimmedText);
        
        parser.setInput(scanner);
        
        parser.splitInput();
        
        parser.parse();
        
        command = parser.getParsed();
        
        parser.reset();
    }
    
    /**
     * Sets a new action to the game to play.
     */
    private static void assembleAction() {
        command.setGame(game);
        
        command.executeCommand();
        
        game = command.getGame();
        
        command = null;
    }
    
}
