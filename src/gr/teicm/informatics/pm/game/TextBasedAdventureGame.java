/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game;

/**
 * This class of objects modify the status of the <code>Context</code> object
 * according to the <code>AbstractAction</code> which is performed.
 * 
 * @author Stamos
 */
public class TextBasedAdventureGame extends AbstractGame {
    
    /**
     * Initializes a newly created <code>TextBasedAdventureGame</code> object.
     */
    public TextBasedAdventureGame() {
        super();
    }
    
    @Override
    protected void run() {
        action.setContext(context);
        
        action.performAction();
        
        context = action.getContext();
        
        outcome = context.getCurrentOutcome();
        
        context.resetCurrentOutcome();
        
        action = null;
    }
    
    @Override
    public void finish() {
        System.exit(0);
    }
    
}
