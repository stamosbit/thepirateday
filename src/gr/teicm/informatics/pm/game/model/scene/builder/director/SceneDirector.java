/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.scene.builder.director;

import gr.teicm.informatics.pm.game.model.builder.director.Director;
import gr.teicm.informatics.pm.game.model.scene.Scene;
import gr.teicm.informatics.pm.game.model.scene.builder.AbstractSceneBuilder;

/**
 * Objects of this class will use a scene builder to create <code>Scene</code>
 * objects.
 * 
 * @author Stamos
 */
public final class SceneDirector extends Director<Scene> {
    
    /**
     * Initializes a newly created <code>SceneDirector</code> object.
     * 
     * @param sceneBuilder
     */
    public SceneDirector(AbstractSceneBuilder sceneBuilder) {
        super(sceneBuilder);
    }
    
}
