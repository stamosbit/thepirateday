/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.scene;

/**
 * This class of objects can be used to create objects that represent a coast
 * scene.
 * 
 * @author Stamos
 */
public class Coast extends Scene {
    
    /**
     * Initializes a new created <code>Coast</code> object.
     * 
     * @param name
     */
    public Coast(String name) {
        super(
                name,
                "This is the coast. It looks like the pirate arrived here."
        );
    }
    
}
