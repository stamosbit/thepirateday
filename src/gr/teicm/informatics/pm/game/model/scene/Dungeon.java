/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.scene;

/**
 * This class of objects can be used to create objects that represent a dungeon
 * scene.
 * 
 * @author Stamos
 */
public class Dungeon extends Scene {
    
    /**
     * Initializes a newly created <code>Dungeon</code> object.
     * 
     * @param name
     */
    public Dungeon(String name) {
        super(
                name,
                "This is a Dungeon. Be ware!"
        );
    }
    
}
