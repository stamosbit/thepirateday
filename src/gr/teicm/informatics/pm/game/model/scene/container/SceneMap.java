/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.scene.container;

import gr.teicm.informatics.pm.game.model.scene.Scene;

import java.util.HashMap;

/**
 * This class of objects support the <code>ISceneContainer</code> interface.
 * 
 * @author Stamos
 */
public class SceneMap implements ISceneContainer {
    
    /**
     * The collection of <code>Scene</code> objects.
     */
    private final HashMap<String,Scene> sceneHashMap;
    
    /**
     * Initializes a newly created <code>SceneMap</code> object.
     */
    public SceneMap() {
        sceneHashMap = new HashMap<>();
    }
    
    @Override
    public HashMap<String,Scene> getAllScenes() {
        return sceneHashMap;
    }
    
    @Override
    public Scene getScene(String key) {
        if (sceneHashMap.containsKey(key)) {
            return sceneHashMap.get(key);
        }
        
        return null;
    }
    
    @Override
    public void putScene(Scene scene) {
        String key = scene.getName();
        
        if (!sceneHashMap.containsKey(key)) {
            sceneHashMap.put(key, scene);
        }
    }
    
    @Override
    public void updateScene(Scene scene) {
        String key = scene.getName();
        
        if (sceneHashMap.containsKey(key)) {
            sceneHashMap.replace(key, scene);
        }
    }
    
    @Override
    public void deleteScene(Scene scene) {
        String key = scene.getName();
        
        if (sceneHashMap.containsKey(key)) {
            sceneHashMap.remove(key, scene);
        }
    }
    
}
