/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.scene.builder;

import gr.teicm.informatics.pm.game.model.scene.SmallForest;

/**
 * This class of objects can be used to create easily <code>SmallForest</code>
 * objects.
 * 
 * @author Stamos
 */
public final class SmallForestBuilder extends AbstractSceneBuilder {
    
    /**
     * Initializes a newly created <code>SmallForestBuilder</code> object.
     */
    public SmallForestBuilder() {
        super(new SmallForest(smallForestName));
    }
    
    @Override
    public void buildActorContainer() {
        // TODO: Put actors.
    }
    
    @Override
    public void buildItemContainer() {
        // TODO: Put items.
    }
    
}
