/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.scene.builder;

import gr.teicm.informatics.pm.game.model.scene.Coast;

/**
 * This class of objects can be used to create easily <code>Coast</code>
 * objects.
 * 
 * @author Stamos
 */
public final class CoastBuilder extends AbstractSceneBuilder {
    
    /**
     * Initializes a newly created <code>CoastBuilder</code> object.
     */
    public CoastBuilder() {
        super(new Coast(coastName));
    }
    
    @Override
    public void buildContainers() {
        buildActorContainer();
        
        buildItemContainer();
    }

    @Override
    public void buildActorContainer() {
        // TODO: Put actors.
    }

    @Override
    public void buildItemContainer() {
        // TODO: Put items.
    }
    
}
