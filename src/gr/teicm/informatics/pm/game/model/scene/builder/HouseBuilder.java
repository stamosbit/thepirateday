/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.scene.builder;

import gr.teicm.informatics.pm.game.model.actor.builder.PlayerBuilder;
import gr.teicm.informatics.pm.game.model.actor.AbstractActor;
import gr.teicm.informatics.pm.game.model.actor.builder.director.ActorDirector;
import gr.teicm.informatics.pm.game.model.item.AbstractItem;
import gr.teicm.informatics.pm.game.model.scene.House;

/**
 * This class of objects can be used to create easily <code>House</code>
 * objects.
 * 
 * @author Stamos
 */
public final class HouseBuilder extends AbstractSceneBuilder {
    
    /**
     * Initializes a newly created <code>HouseBuilder</code> object.
     */
    public HouseBuilder() {
        super(new House(houseName));
    }

    @Override
    public void buildActorContainer() {
        actorDirector = new ActorDirector(new PlayerBuilder());
        
        actorDirector.prepareModel();
        
        AbstractActor player = actorDirector.getModel();
        
        player.setCurrentSceneName(sceneName);
        
        model.putActor(player);
    }

    @Override
    public void buildItemContainer() {
        AbstractItem book = itemFactory.getInstance(bookKey);
        
        AbstractItem chest = itemFactory.getInstance(chestKey);
        
        model.putItem(book);
        
        model.putItem(chest);
    }
    
}
