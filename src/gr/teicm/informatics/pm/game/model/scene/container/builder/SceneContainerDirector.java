/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.scene.container.builder;

import gr.teicm.informatics.pm.game.builder.IContainerBuilder;
import gr.teicm.informatics.pm.game.model.scene.container.ISceneContainer;

/**
 * This is a director class which is used to prepare(build)
 * <code>ISceneContainer</code> objects and return them using an hiearchy of
 * <code>SceneContainerBuilder</code> objects.
 * 
 * @author Stamos
 */
public final class SceneContainerDirector {
    
    private final IContainerBuilder<ISceneContainer> sceneContainerBuilder;
    
    /**
     * Initializes a newly created <code>SceneContainerDirector</code> object.
     */
    public SceneContainerDirector() {
        sceneContainerBuilder = new SceneContainerBuilder();
    }
    
    /**
     * Builds a <code>SceneContainer</code> object using the <tt>build()</tt>
     * methods of the scene-container builder.
     */
    public void prepareInstance() {
        // Call below any existing build method of the builder.
        sceneContainerBuilder.buildContainer();
    }
    
    /**
     * Returns the <code>ISceneContainer</code> built instance.
     * 
     * @return the ISceneContainer built instance
     */
    public ISceneContainer getInstance() {
        return sceneContainerBuilder.getInstance();
    }
    
}
