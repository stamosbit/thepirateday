/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.scene;

/**
 * This class of objects can be used to create objects that represent a big
 * forest scene.
 * 
 * @author Stamos
 */
public class BigForest extends Scene {
    
    /**
     * Initializes a newly created <code>BigForest</code> object.
     * 
     * @param name
     */
    public BigForest(String name) {
        super(
                name,
                "This is a big forest. You can hear wild animals roaring! Be ware!"
        );
    }
    
}
