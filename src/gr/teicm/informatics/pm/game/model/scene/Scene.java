/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.scene;

import gr.teicm.informatics.pm.game.model.GameComponent;
import gr.teicm.informatics.pm.game.model.actor.AbstractActor;
import gr.teicm.informatics.pm.game.model.item.AbstractItem;
import gr.teicm.informatics.pm.game.model.actor.container.IActorContainer;
import gr.teicm.informatics.pm.game.model.item.container.IItemContainer;
import gr.teicm.informatics.pm.game.model.actor.container.ActorMap;
import gr.teicm.informatics.pm.game.model.item.container.ItemMap;

import java.util.HashMap;

/**
 * This class has the purpose to exist as a superclass to every object that will
 * behave like a scene in the game.
 * 
 * @author Stamos
 */
public class Scene extends GameComponent
implements IActorContainer, IItemContainer {
    
    /**
     * Indicates the NORTH <code>Scene</code> object's <tt>name</tt>.
     */
    protected String northSceneName;
    
    /**
     * Indicates the SOUTH <code>Scene</code> object's <tt>name</tt>.
     */
    protected String southSceneName;
    
    /**
     * Indicates the WEST <code>Scene</code> object's <tt>name</tt>.
     */
    protected String westSceneName;
    
    /**
     * Indicates the EAST <code>Scene</code> object's <tt>name</tt>.
     */
    protected String eastSceneName;
    
    /**
     * Stores all the <code>AbstractActor</code> objects that will be contained.
     */
    protected final ActorMap actorMap;
    
    /**
     * Stores all the <code>AbstractItem</code> objects that will be contained.
     */
    protected final ItemMap itemMap;
    
    /**
     * Initializes a newly created <code>Scene</code> object. Use this
     * constructor from subclasses.
     * 
     * @param name
     * @param description
     */
    protected Scene(String name, String description) {
        super(name, description);
        
        northSceneName = "";
        
        southSceneName = "";
        
        westSceneName = "";
        
        eastSceneName = "";
        
        actorMap = new ActorMap();
        
        itemMap = new ItemMap();
    }
    
    /**
     * Returns the NORTH <code>Scene</code> object's name.
     * 
     * @return NORTH Scene object's name
     */
    public String getNorthSceneName() {
        return northSceneName;
    }
    
    /**
     * Replaces the NORTH <code>Scene</code> object's name.
     * 
     * @param northSceneName
     */
    public void setNorthSceneName(String northSceneName) {
        this.northSceneName = northSceneName;
    }
    
    /**
     * Returns the SOUTH <code>Scene</code> object's name.
     * 
     * @return SOUTH Scene object's name
     */
    public String getSouthSceneName() {
        return southSceneName;
    }
    
    /**
     * Replaces the SOUTH <code>Scene</code> object's name.
     * 
     * @param southSceneName
     */
    public void setSouthSceneName(String southSceneName) {
        this.southSceneName = southSceneName;
    }
    
    /**
     * Returns the WEST <code>Scene</code> object's name.
     * 
     * @return WEST Scene object's name
     */
    public String getWestSceneName() {
        return westSceneName;
    }
    
    /**
     * Replaces the WEST <code>Scene</code> object's name.
     * 
     * @param westSceneName
     */
    public void setWestSceneName(String westSceneName) {
        this.westSceneName = westSceneName;
    }
    
    /**
     * Returns the EAST <code>Scene</code> object's name.
     * 
     * @return EAST Scene object's name
     */
    public String getEastSceneName() {
        return eastSceneName;
    }
    
    /**
     * Replaces the EAST <code>Scene</code> object's name.
     * 
     * @param eastScene
     */
    public void setEastSceneName(String eastScene) {
        this.eastSceneName = eastScene;
    }
    
    @Override
    public HashMap<String,AbstractActor> getAllActors() {
        HashMap<String,AbstractActor> actorHashMap = actorMap.getAllActors();
        
        return actorHashMap;
    }
    
    @Override
    public AbstractActor getActor(String key) {
        AbstractActor actor = actorMap.getActor(key);
        
        return actor;
    }
    
    @Override
    public void putActor(AbstractActor actor) {
        actorMap.putActor(actor);
    }
    
    @Override
    public void updateActor(AbstractActor actor) {
        actorMap.updateActor(actor);
    }
    
    @Override
    public void deleteActor(AbstractActor actor) {
        actorMap.deleteActor(actor);
    }
    
    @Override
    public HashMap<String,AbstractItem> getAllItems() {
        HashMap<String,AbstractItem> itemHashMap = itemMap.getAllItems();
        
        return itemHashMap;
    }
    
    @Override
    public AbstractItem getItem(String key) {
        AbstractItem item = itemMap.getItem(key);
        
        return item;
    }
    
    @Override
    public void putItem(AbstractItem item) {
        itemMap.putItem(item);
    }
    
    @Override
    public void updateItem(AbstractItem item) {
        itemMap.updateItem(item);
    }
    
    @Override
    public void deleteItem(AbstractItem item) {
        itemMap.deleteItem(item);
    }
    
}
