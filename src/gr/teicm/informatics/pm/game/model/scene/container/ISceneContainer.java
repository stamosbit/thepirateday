/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.scene.container;

import gr.teicm.informatics.pm.game.model.scene.Scene;

import java.util.HashMap;

/**
 * This interface can be implemented by classes that contain
 * <code>Scene</code> objects.
 * 
 * @author Stamos
 */
public interface ISceneContainer extends java.io.Serializable {
    
    /**
     * Returns the collection of <code>Scene</code> objects.
     * 
     * @return the collection of Scene objects
     */
    HashMap<String,Scene> getAllScenes();
    
    /**
     * Returns an <code>Scene</code> object if the collection contains an entry
     * named with <code>key</code>.
     * 
     * @param key
     *        the name of the prefered Scene object
     * @return Scene object
     */
    Scene getScene(String key);
    
    /**
     * Puts <code>scene</code> into the collection if an entry like that is
     * <tt>not</tt> already contained inside the collection.
     * 
     * @param scene
     *        the Scene object to be put into the collection
     */
    void putScene(Scene scene);
    
    /**
     * Updates <code>scene</code> in the collection if an entry like that is
     * already contained inside the collection.
     * 
     * @param scene
     *        the Scene object to be updated in the collection
     */
    void updateScene(Scene scene);
    
    /**
     * Deletes <code>scene</code> from the collection if an entry like that is
     * already contained inside the collection.
     * 
     * @param scene
     *        the Scene object to be deleted from the collection
     */
    void deleteScene(Scene scene);
    
}
