/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.scene.builder;

import gr.teicm.informatics.pm.game.model.scene.BigForest;

/**
 * This class of objects can be used to create easily <code>BigForest</code>
 * objects.
 * 
 * @author Stamos
 */
public final class BigForestBuilder extends AbstractSceneBuilder {
    
    /**
     * Initializes a newly created <code>BigForestBuilder</code> object.
     */
    public BigForestBuilder() {
        super(new BigForest(bigForestName));
    }
    
    @Override
    public void buildActorContainer() {
        // TODO: Put actors.
    }
    
    @Override
    public void buildItemContainer() {
        // TODO: Put items.
    }
}
