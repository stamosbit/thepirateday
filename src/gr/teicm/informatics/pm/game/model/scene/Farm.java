/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.scene;

/**
 * This class of objects can be used to create objects that represent a farm
 * scene.
 * 
 * @author Stamos
 */
public class Farm extends Scene {
    
    /**
     * Initializes a newly created <code>Farm</code> object.
     * 
     * @param name
     */
    public Farm(String name) {
        super(
                name,
                "This is a farm. You can milk cows for potions."
        );
    }
    
}
