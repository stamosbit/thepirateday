/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.scene.builder;

import gr.teicm.informatics.pm.game.model.actor.builder.PirateBuilder;
import gr.teicm.informatics.pm.game.model.actor.AbstractActor;
import gr.teicm.informatics.pm.game.model.actor.builder.director.ActorDirector;
import gr.teicm.informatics.pm.game.model.scene.Dungeon;

/**
 * This class of objects can be used to create easily <code>Dungeon</code>
 * objects.
 * 
 * @author Stamos
 */
public final class DungeonBuilder extends AbstractSceneBuilder {
    
    /**
     * Initializes a newly created <code>DungeonBuilder</code> object.
     */
    public DungeonBuilder() {
        super(new Dungeon(dungeonName));
    }
    
    @Override
    public void buildActorContainer() {
        actorDirector = new ActorDirector(new PirateBuilder());
        
        actorDirector.prepareModel();
        
        AbstractActor pirate = actorDirector.getModel();
        
        pirate.setCurrentSceneName(sceneName);
        
        model.putActor(pirate);
    }
    
    @Override
    public void buildItemContainer() {
        // TODO: Put items.
    }
    
}
