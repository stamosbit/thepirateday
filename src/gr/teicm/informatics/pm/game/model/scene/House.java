/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.scene;

/**
 * This class of objects can be used to create objects that represent a house
 * scene.
 * 
 * @author Stamos
 */
public class House extends Scene {
    
    /**
     * Initializes a newly created <code>House</code> object.
     * 
     * @param name
     */
    public House(String name) {
        super(
                name,
                "This is your house. You can try going to a direction."
        );
    }
    
}
