/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.scene.builder;

import gr.teicm.informatics.pm.game.model.builder.AbstractModelBuilder;
import gr.teicm.informatics.pm.game.constant.SceneConstants;
import gr.teicm.informatics.pm.game.model.actor.AbstractActor;
import gr.teicm.informatics.pm.game.model.builder.director.IDirector;
import gr.teicm.informatics.pm.game.model.scene.Scene;

/**
 * This <code>abstract</code> class is extended from any class that is needed to
 * create a different kind of <code>Scene</code> object builder. Its purpose is
 * to make the creation of the initial state of a <code>Scene</code> object
 * easier and simpler.
 * 
 * @author Stamos
 */
public abstract class AbstractSceneBuilder
extends AbstractModelBuilder<Scene> {
    
    protected static final String houseName =
            SceneConstants.HOUSE.name().toLowerCase();
    
    protected static final String smallForestName =
            SceneConstants.SMALL_FOREST.name().toLowerCase();
    
    protected static final String farmName =
            SceneConstants.FARM.name().toLowerCase();
    
    protected static final String bigForestName =
            SceneConstants.BIG_FOREST.name().toLowerCase();
    
    protected static final String dungeonName =
            SceneConstants.DUNGEON.name().toLowerCase();
    
    protected static final String coastName =
            SceneConstants.COAST.name().toLowerCase();
    
    /**
     * The model's name which will be set in all <code>AbstractActor</code>
 objects before they are put in the model's actor container.
     */
    protected final String sceneName;
    
    /**
     * The actor director which will return prepare <code>AbstractActor</code>
 instances and return them for the model's actor container.
     */
    protected IDirector<AbstractActor> actorDirector;
    
    /**
     * Initializes a newly created <code>AbstractSceneBuilder</code> object. Use
     * this constructor from subclasses.
     * 
     * @param model
     */
    protected AbstractSceneBuilder(Scene model) {
        super(model);
        
        sceneName = this.model.getName();
    }
    
    @Override
    public void buildContainers() {
        buildItemContainer();
        
        buildActorContainer();
    }
    
    /**
     * Builds the model's actor container using an actor director.
     */
    public abstract void buildActorContainer();
    
}
