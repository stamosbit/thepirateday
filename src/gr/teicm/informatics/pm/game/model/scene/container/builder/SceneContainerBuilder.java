/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.scene.container.builder;

import gr.teicm.informatics.pm.game.model.scene.builder.director.SceneDirector;
import gr.teicm.informatics.pm.game.builder.IContainerBuilder;
import gr.teicm.informatics.pm.game.model.builder.director.IDirector;
import gr.teicm.informatics.pm.game.model.scene.builder.*;
import gr.teicm.informatics.pm.game.model.scene.container.*;
import gr.teicm.informatics.pm.game.model.scene.Scene;

/**
 * This class of objects create a collection of <code>Scene</code> objects and
 * return it.
 * 
 * @author Stamos
 */
public final class SceneContainerBuilder
implements IContainerBuilder<ISceneContainer> {
    
    private final ISceneContainer sceneContainer;
    
    private IDirector<Scene> sceneDirector;
    
    /**
     * Initializes a newly created <code>SceneContainerBuilder</code> object.
     */
    public SceneContainerBuilder() {
        sceneContainer = new SceneMap();
    }
    
    @Override
    public void buildContainer() {
        sceneDirector = new SceneDirector(new HouseBuilder());
        
        sceneDirector.prepareModel();
        
        Scene house = sceneDirector.getModel();
        
        sceneDirector = new SceneDirector(new SmallForestBuilder());
        
        sceneDirector.prepareModel();
        
        Scene smallForest = sceneDirector.getModel();
        
        sceneDirector = new SceneDirector(new FarmBuilder());
        
        sceneDirector.prepareModel();
        
        Scene farm = sceneDirector.getModel();
        
        sceneDirector = new SceneDirector(new BigForestBuilder());
        
        sceneDirector.prepareModel();
        
        Scene bigForest = sceneDirector.getModel();
        
        sceneDirector = new SceneDirector(new DungeonBuilder());
        
        sceneDirector.prepareModel();
        
        Scene dungeon = sceneDirector.getModel();
        
        sceneDirector = new SceneDirector(new CoastBuilder());
        
        sceneDirector.prepareModel();
        
        Scene coast = sceneDirector.getModel();
        
        house = buildSceneRelationships(
                house,
                smallForest.getName(),
                "",
                farm.getName(),
                bigForest.getName()
        );
        
        smallForest = buildSceneRelationships(
                smallForest,
                dungeon.getName(),
                house.getName(),
                "",
                ""
        );
        
        farm = buildSceneRelationships(
                farm,
                "",
                "",
                "",
                house.getName()
        );
        
        bigForest = buildSceneRelationships(
                bigForest,
                "",
                "",
                house.getName(),
                ""
        );
        
        dungeon = buildSceneRelationships(
                dungeon,
                "",
                smallForest.getName(),
                "",
                coast.getName()
        );
        
        coast = buildSceneRelationships(
                coast,
                "",
                "",
                dungeon.getName(),
                ""
        );
        
        sceneContainer.putScene(house);
        
        sceneContainer.putScene(smallForest);
        
        sceneContainer.putScene(farm);
        
        sceneContainer.putScene(bigForest);
        
        sceneContainer.putScene(dungeon);
        
        sceneContainer.putScene(coast);
    }
    
    private Scene buildSceneRelationships(
            Scene scene,
            String northSceneName,
            String southSceneName,
            String westSceneName,
            String eastSceneName
    ) {
        scene.setNorthSceneName(northSceneName);
        
        scene.setSouthSceneName(southSceneName);
        
        scene.setWestSceneName(westSceneName);
        
        scene.setEastSceneName(eastSceneName);
        
        return scene;
    }
    
    @Override
    public ISceneContainer getInstance() {
        return sceneContainer;
    }
    
}
