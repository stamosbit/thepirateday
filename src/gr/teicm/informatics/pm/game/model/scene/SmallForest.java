/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.scene;

/**
 * This class of objects can be used to create objects that represent a small
 * forest scene.
 * 
 * @author Stamos
 */
public class SmallForest extends Scene  {
    
    /**
     * Initializes a newly created <code>SmallForest</code> object.
     * 
     * @param name
     */
    public SmallForest(String name) {
        super(
                name,
                "This is a small forest. You can hear the birds tweeting."
        );
    }
    
}
