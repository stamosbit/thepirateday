/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model;

import gr.teicm.informatics.pm.game.model.item.AbstractItem;

/**
 *
 * @author Stamos
 */
public interface ITakeable extends java.io.Serializable {
    
    void take(AbstractItem item);
    
}
