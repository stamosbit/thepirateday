/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model;

/**
 *
 * @author Stamos
 */
public interface IOpening extends java.io.Serializable {
    
    /**
     * Returns <tt>true</tt> if the model is opened.
     * 
     * @return boolean value
     */
    Boolean isOpened();
    
    /**
     * The model is made opened.
     */
    void makeOpened();
    
    /**
     * If the model is made closed.
     */
    void makeClosed();
    
}
