/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.builder;

import gr.teicm.informatics.pm.game.constant.ItemConstants;
import gr.teicm.informatics.pm.game.factory.IFactory;
import gr.teicm.informatics.pm.game.model.item.AbstractItem;
import gr.teicm.informatics.pm.game.model.item.factory.ItemFactory;

/**
 * This <code>abstract</code> class can be extended from any class whose objects
 * need to create a different kind of game component builder. It contains an
 * item factory which will provide any model builder the ability to build an
 * item container much easier.
 * 
 * @author Stamos
 * @param <T>
 */
public abstract class AbstractModelBuilder<T> implements IModelBuilder<T> {
    
    protected static final String potionKey =
            ItemConstants.POTION.name().toLowerCase();
    
    protected static final String drugKey =
            ItemConstants.DRUG.name().toLowerCase();
    
    protected static final String knifeKey =
            ItemConstants.KNIFE.name().toLowerCase();
    
    protected static final String pistolKey =
            ItemConstants.PISTOL.name().toLowerCase();
    
    protected static final String bookKey =
            ItemConstants.BOOK.name().toLowerCase();
    
    protected static final String chestKey =
            ItemConstants.CHEST.name().toLowerCase();
    
    protected static final String doorKey =
            ItemConstants.DOOR.name().toLowerCase();
    
    protected static final String cocaineKey =
            ItemConstants.COCAINE.name().toLowerCase();
    
    /**
     * The factory that will make item instances initialization simpler.
     */
    protected static IFactory<AbstractItem> itemFactory = new ItemFactory();
    
    protected final T model;
    
    /**
     * Initializes a newly created <code>AbstractGameComponentBuilder</code>
     * object. Use this constructor from subclasses.
     * 
     * @param model
     */
    protected AbstractModelBuilder(T model) {
        this.model = model;
    }
    
    /**
     * Builds a <code>GameComponent</code> object's item container using an item
     * factory.
     */
    public abstract void buildItemContainer();
    
    @Override
    public T getModel() {
        return model;
    }
    
}
