/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.builder.director;

/**
 * This interface can be implemented by classes whose objects can be directors
 * of builders.
 * 
 * @author Stamos
 * @param <T>
 */
public interface IDirector<T> {
    
    /**
     * Calls all building methods of the builder.
     */
    void prepareModel();
    
    /**
     * Returns the prepared model.
     * 
     * @return
     */
    T getModel();
    
}
