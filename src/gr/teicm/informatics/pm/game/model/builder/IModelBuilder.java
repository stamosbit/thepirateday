/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.builder;

/**
 * This interface can be implemented by classes whose objects will create
 * builders that create game components. Another option is for those classes
 * to extend the <code>AbstractModelBuilder</code> class.
 * 
 * @author Stamos
 * @param <T>
 */
public interface IModelBuilder<T> {
    
    /**
     * This method is used to apply easier polymorphism for any builder
     * subclasses. It builds items or/and actors and adds them to the model's
     * containers when needed.
     */
    void buildContainers();
    
    /**
     * Returns the build model.
     * 
     * @return the built model.
     */
    T getModel();
    
}
