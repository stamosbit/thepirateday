/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.builder.director;

import gr.teicm.informatics.pm.game.model.builder.IModelBuilder;

/**
 * This class has the purpose to support the <code>IDirector</code> interface.
 * 
 * @author Stamos
 * @param <T>
 */
public class Director<T> implements IDirector<T> {
    
    /**
     * The builder which will be used to build models and their containers.
     */
    protected IModelBuilder builder;
    
    /**
     * Initializes a newly created <code>Director</code> object. Use this
     * constructor from subclasses.
     * 
     * @param builder
     */
    protected Director(IModelBuilder builder) {
        this.builder = builder;
    }
    
    @Override
    public void prepareModel() {
        builder.buildContainers();
    }
    
    @Override
    public T getModel() {
        return (T) builder.getModel();
    }
    
}
