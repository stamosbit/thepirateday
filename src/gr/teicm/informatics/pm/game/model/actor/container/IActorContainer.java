/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.actor.container;

import gr.teicm.informatics.pm.game.model.actor.AbstractActor;

import java.util.HashMap;

/**
 * This interface can be implemented by classes that contain
 * <code>AbstractActor</code> objects.
 * 
 * @author Stamos
 */
public interface IActorContainer extends java.io.Serializable {
    
    /**
     * Returns the collection of <code>AbstractActor</code> objects.
     * 
     * @return the collection of AbstractActor objects
     */
    HashMap<String,AbstractActor> getAllActors();
    
    /**
     * Returns an <code>AbstractActor</code> object if the collection contains
     * an entry keyed with <code>key</code>.
     * 
     * @param key
     *        the name of the prefered AbstractActor object
     * @return an AbstractActor object
     */
    AbstractActor getActor(String key);
    
    /**
     * Puts <code>actor</code> into the collection if an entry like that is
     * <tt>not</tt> already contained inside collection.
     * 
     * @param actor
     *        the AbstractActor object to be put into the collection
     */
    void putActor(AbstractActor actor);
    
    /**
     * Updates <code>actor</code> in the collection if an entry like that is
     * already contained inside the collection.
     * 
     * @param actor
     *        the AbstractActor object to be updated in the colletion
     */
    void updateActor(AbstractActor actor);
    
    /**
     * Deletes <code>actor</code> from the collection if an entry like that is
     * contained inside the collection.
     * 
     * @param actor
     *        the AbstractActor object to be deleted from the collection
     */
    void deleteActor(AbstractActor actor);
    
}
