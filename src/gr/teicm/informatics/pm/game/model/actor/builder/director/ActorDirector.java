/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.actor.builder.director;

import gr.teicm.informatics.pm.game.model.actor.AbstractActor;
import gr.teicm.informatics.pm.game.model.actor.builder.AbstractActorBuilder;
import gr.teicm.informatics.pm.game.model.builder.director.Director;

/**
 * Objects of this class will use an actor builder to create
 * <code>AbstractActor</code> objects.
 * 
 * @author Stamos
 */
public final class ActorDirector extends Director<AbstractActor> {
    
    /**
     * Initializes a newly created <code>ActorDirector</code> object.
     * 
     * @param builder
     */
    public ActorDirector(AbstractActorBuilder builder) {
        super(builder);
    }
    
}
