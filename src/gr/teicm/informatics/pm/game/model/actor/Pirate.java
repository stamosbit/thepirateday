/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.actor;

import gr.teicm.informatics.pm.game.model.Inventory;

/**
 * This class of object represents pirates.
 * 
 * @author Stamos
 */
public class Pirate extends AbstractActor {
    
    /**
     * Initializes a newly created <tt>Pirate</tt> object.
     * 
     * @param name
     */
    public Pirate(String name) {
        super(name, "This is a pirate. He is very fearsome.");
        healthPoints = 75;
        
        inventory = new Inventory();
    }
    
    @Override
    public Integer stab() {
        Integer damage = knife.getDamageDone();
        
        return damage;
    }
    
    @Override
    public Integer shoot() {
        Integer damage = pistol.getDamageDone();
        
        return damage;
    }
    
}
