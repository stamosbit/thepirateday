/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.actor.builder;

import gr.teicm.informatics.pm.game.model.actor.Player;
import gr.teicm.informatics.pm.game.model.item.AbstractItem;
import gr.teicm.informatics.pm.game.model.item.weapon.AbstractWeapon;

/**
 * This class of objects can be used to easily create <code>Player</code>
 * objects.
 * 
 * @author Stamos
 */
public final class PlayerBuilder extends AbstractActorBuilder {
    
    /**
     * Initializes a newly created <code>PlayerBuilder</code> object.
     */
    public PlayerBuilder() {
        super(new Player(playerName));
    }
    
    @Override
    public void buildItemContainer() {
        AbstractItem potion = itemFactory.getInstance(potionKey);
        
        AbstractItem book = itemFactory.getInstance(bookKey);
        
        model.putItem(potion);
        
        model.putItem(book);
    }
    
    @Override
    public void buildWeapons() {
        AbstractWeapon knife =
                (AbstractWeapon) itemFactory.getInstance(knifeKey);
        
        AbstractWeapon pistol =
                (AbstractWeapon) itemFactory.getInstance(pistolKey);
        
        model.setKnife(knife);
        
        model.setPistol(pistol);
    }
    
}
