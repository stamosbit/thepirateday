/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.actor.builder;

import gr.teicm.informatics.pm.game.model.builder.AbstractModelBuilder;
import gr.teicm.informatics.pm.game.constant.ActorConstants;
import gr.teicm.informatics.pm.game.model.actor.AbstractActor;

/**
 * This <code>abstract</code> class is extended from any class that is needed to
 * create a different kind of <code>AbstractActor</code> object builder. Its
 * purpose is to make the creation of the initial state of an
 * <code>AbstractActor</code> object easier and simpler.
 * 
 * @author Stamos
 */
public abstract class AbstractActorBuilder
extends AbstractModelBuilder<AbstractActor> {
    
    protected static final String playerName =
            ActorConstants.PLAYER.name().toLowerCase();
    
    protected static final String pirateName =
            ActorConstants.PIRATE.name().toLowerCase();
    
    /**
     * Initializes a newly created <code>AbstractActorBuilder</code> object. Use
     * this constructor from subclasses.
     * 
     * @param model
     *        the AbstractActor object that will be built
     */
    protected AbstractActorBuilder(AbstractActor model) {
        super(model);
    }
    
    @Override
    public void buildContainers() {
        buildItemContainer();
        buildWeapons();
    }
    
    /**
     * Builds the weapons of an <code>AbstractActor</code> object with the
     * support the item factory.
     */
    public abstract void buildWeapons();
    
}
