/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.actor.builder;

import gr.teicm.informatics.pm.game.model.builder.AbstractModelBuilder;
import gr.teicm.informatics.pm.game.model.actor.Pirate;
import gr.teicm.informatics.pm.game.model.item.AbstractItem;
import gr.teicm.informatics.pm.game.model.item.weapon.AbstractWeapon;

/**
 * This class of objects can be used to create easily <code>Pirate</code>
 * objects.
 * 
 * @author Stamos
 */
public final class PirateBuilder extends AbstractActorBuilder {
    
    /**
     * Initializes a newly created <code>PirateBuilder</code> object.
     */
    public PirateBuilder() {
        super(new Pirate(pirateName));
    }
    
    @Override
    public void buildItemContainer() {
        AbstractItem potion = itemFactory.getInstance(AbstractModelBuilder.potionKey);
        
        model.putItem(potion);
    }
    
    @Override
    public void buildWeapons() {
        AbstractWeapon knife = (AbstractWeapon) itemFactory.getInstance(knifeKey);
        
        AbstractWeapon pistol = (AbstractWeapon) itemFactory.getInstance(pistolKey);
        
        model.setKnife(knife);
        
        model.setPistol(pistol);
    }
    
}
