/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.actor.container;

import gr.teicm.informatics.pm.game.model.actor.AbstractActor;

import java.util.HashMap;

/**
 * This class of objects support the <code>IActorContainer</code> interface.
 * 
 * @author Stamos
 */
public class ActorMap implements IActorContainer {
    
    /**
     * The collection of <code>AbstractActor</code> objects.
     */
    private final HashMap<String,AbstractActor> actorHashMap;
    
    /**
     * Initializes a newly created <code>ActorMap</code> object.
     */
    public ActorMap() {
        actorHashMap = new HashMap<>();
    }
    
    @Override
    public HashMap<String,AbstractActor> getAllActors() {
        return actorHashMap;
    }
    
    @Override
    public AbstractActor getActor(String key) {
        if (actorHashMap.containsKey(key)) {
            return actorHashMap.get(key);
        }
        
        return null;
    }
    
    @Override
    public void putActor(AbstractActor actor) {
        String key = actor.getName();
        
        if (!actorHashMap.containsKey(key)) {
            actorHashMap.put(key, actor);
        }
    }
    
    @Override
    public void updateActor(AbstractActor actor) {
        String key = actor.getName();
        
        if (actorHashMap.containsKey(key)) {
            actorHashMap.replace(key, actor);
        }
    }
    
    @Override
    public void deleteActor(AbstractActor actor) {
        String key = actor.getName();
        
        if (actorHashMap.containsKey(key)) {
            actorHashMap.remove(key, actor);
        }
    }
    
}
