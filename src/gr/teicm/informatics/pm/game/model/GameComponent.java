/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model;

/**
 * This class is extended by all model classes.
 * 
 * @author Stamos
 */
public class GameComponent implements java.io.Serializable {
    
    /**
     * Indicates the object's name.
     */
    protected String name;
    
    /**
     * Describes the object.
     */
    protected String description;
    
    /**
     * Initializes a newly created <code>GameComponent</code> object. Use this
     * constructor from subclasses.
     * 
     * @param name
     *        the object's name
     * @param description
     *        the object's description
     */
    protected GameComponent(String name, String description) {
        this.name = name;
        
        this.description = description;
    }
    
    /**
     * Returns object's name.
     * 
     * @return name
     *         the object's name
     */
    public String getName() {
        return name;
    }
    
    /**
     * Sets a newly value to <code>name</code>.
     * 
     * @param name
     *        the object's new name value
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Returns the object's description.
     * 
     * @return description
     *         the object's description
     */
    public String getDescription() {
        return description;
    }
    
    /**
     * Sets a newly value to <code>description</code>.
     * 
     * @param description
     *        the object's new description
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
}
