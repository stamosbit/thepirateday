/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.item;

import gr.teicm.informatics.pm.game.model.GameComponent;

/**
 * The superclass of all items.
 * 
 * @author Stamos
 * @param <T>
 */
public abstract class AbstractItem<T> extends GameComponent {
    
    /**
     * Holds the name of the <tt>AbstractActor</tt> object which is contained
     * into.
     */
    protected String carrierActorName;
    
    /**
     * Initializes a newly created <code>AbstractItem</code> object. Use this
     * constructor from subclasses.
     * 
     * @param name
     * @param description
     */
    protected AbstractItem(String name, String description) {
        super(name, description);
        
        carrierActorName = "";
    }
    
    /**
     * Returns <tt>true</tt> if this <code>AbstractItem</code> object is
     * takable.
     * 
     * @return a boolean value
     */
    public abstract boolean isTakable();
    
    /**
     * Returns a value depending on the <code>AbstractItem</code> object that is
     * used.
     * 
     * @return a value depending on the AbstractItem object that is used
     */
    public abstract T use();
    
    /**
     * Returns the <code>AbstractActor</code> object's <tt>name</tt> which
     * indicates who is carring the item.
     * 
     * @return Actor object's name
     */
    public String getCarrierActorName() {
        return carrierActorName;
    }
    
    /**
     * Sets a new value to the <code>AbstractActor</code> object's name which
     * indicates who is carring the item.
     * 
     * @param carrierActorName
     */
    public void setCarrierActorName(String carrierActorName) {
        this.carrierActorName = carrierActorName;
    }
    
}
