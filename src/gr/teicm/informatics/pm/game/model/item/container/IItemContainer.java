/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.item.container;

import gr.teicm.informatics.pm.game.model.item.AbstractItem;

import java.util.HashMap;

/**
 * This interface can be implemented by classes that contain
 * <code>AbstractItem</code> objects.
 * 
 * @author Stamos
 */
public interface IItemContainer extends java.io.Serializable {
    
    /**
     * Returns the collection of <tt>AbstractItem</tt> objects.
     * 
     * @return the collection of AbstractItem objects
     */
    HashMap<String,AbstractItem> getAllItems();
    
    /**
     * Returns an <code>AbstractItem</code> object if the collection contains an
     * entry named with <code>key</code>.
     * 
     * @param key
     *        the name of the prefered AbstractItem object
     * @return item object
     */
    AbstractItem getItem(String key);
    
    /**
     * Puts <code>item</code> into the collection if an entry like that is
     * <tt>not</tt> already contained inside the collection.
     * 
     * @param item
     *        the AbstractItem object to be put into the collection
     */
    void putItem(AbstractItem item);
    
    /**
     * Updates <code>item</code> in the collection if an entry like that is
     * already contained inside the collection.
     * 
     * @param item
     *        the AbstractItem object to be updated in the collection
     */
    void updateItem(AbstractItem item);
    
    /**
     * Deletes <code>item</code> from the collection if an entry like that is
     * already contained inside the collection.
     * 
     * @param item
     *        the AbstractItem object to be deleted from the collection
     */
    void deleteItem(AbstractItem item);
    
}
