/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.item.consumable;

/**
 *
 * @author Stamos
 */
public class Potion extends AbstractConsumableItem {
    
    /**
     * Indicates the amount of amountOfHealing when used.
     */
    private final Integer amountOfHealing;
    
    /**
     * Initializes a newly created <code>Potion</code> object.
     * @param name
     */
    public Potion(String name) {
        super(name, "This is a potion. You can use it to heal yourself.");
        
        amountOfHealing = 25;
    }
    
    public Integer getAmountOfHealing() {
        return amountOfHealing;
    }
    
    @Override
    public boolean isTakable() {
        return true;
    }
    
}
