/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.item.miscellaneous;

/**
 * The book class of the game.
 * 
 * @author Stamos
 */
public class Book extends AbstractMiscellaneous<String> {
    
    protected String context;

    /**
     * Initializes a newly created <code>Book</code> object.
     * 
     * @param name
     */
    public Book(String name) {
        super(name, "This is a book. You can read it.");
    }
    
    @Override
    public boolean isTakable() {
        return true;
    }
    
    public String getContext() {
        return context;
    }
    
    @Override
    public String use() {
        return getContext();
    }
    
}
