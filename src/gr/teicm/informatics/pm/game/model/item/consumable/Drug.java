/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.item.consumable;

import java.util.Random;

/**
 *
 * @author Stamos
 */
public class Drug extends AbstractConsumableItem {
    
    private final Integer[] amounts = {-10, -5, 0, 5, 10};
    
    private Random random;
    
    /**
     * Initializes a newly created <code>Drug</code> object.
     * 
     * @param name
     */
    public Drug(String name) {
        super(
                name,
                "This is a drug. " +
                "You can use it to get a random effect to yourself."
        );
    }
    
    @Override
    public Integer getAmountOfEffect() {
        randomizeAmountOfEffect();
        
        return amountOfEffect;
    }
    
    private void randomizeAmountOfEffect() {
        Integer bound = amounts.length - 1;
        
        random = new Random();
        
        Integer randomIndex = random.nextInt(bound);
        
        amountOfEffect = amounts[randomIndex];
    }

    @Override
    public boolean isTakable() {
        return true;
    }
    
}
