/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.item.weapon;

/**
 * The pistol class of the game.
 * 
 * @author Stamos
 */
public class Pistol extends AbstractWeapon {
    
    /**
     * Initializes a newly created <code>Pistol</code> object.
     * 
     * @param name
     */
    public Pistol(String name) {
        super(name, "This is a pistol. You can shoot someone with it.");
        
        damageDone = 10;
    }
    
    @Override
    public boolean isTakable() {
        return true;
    }

    @Override
    public Integer attack() {
        return damageDone;
    }
    
}
