/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.item.miscellaneous;

import gr.teicm.informatics.pm.game.model.item.AbstractItem;
import gr.teicm.informatics.pm.game.model.item.container.IItemContainer;
import gr.teicm.informatics.pm.game.model.IOpening;
import gr.teicm.informatics.pm.game.model.item.container.ItemMap;

import java.util.HashMap;

/**
 * The chest class of the game which can contain other items.
 * 
 * @author Stamos
 */
public class Chest extends AbstractMiscellaneous<Boolean>
implements IOpening, IItemContainer {
    
    /**
     * Indicates if the chest is opened for access.
     */
    private boolean opened;
    
    /**
     * Stores all the <tt>item</tt> objects that will be contained.
     */
    private final IItemContainer itemContainer;
    
    /**
     * Initializes a newly created <code>Chest</code> object.
     * 
     * @param name
     */
    public Chest(String name) {
        super(name, "This is a chest. It can contain items.");
        
        opened = false;
        
        itemContainer = new ItemMap();
    }
    
    @Override
    public boolean isTakable() {
        return false;
    }
    
    @Override
    public Boolean isOpened() {
        return opened;
    }
    
    @Override
    public void makeOpened() {
        opened = true;
    }
    
    @Override
    public void makeClosed() {
        opened = false;
    }
    
    @Override
    public HashMap<String,AbstractItem> getAllItems() {
        HashMap<String,AbstractItem> itemHashMap = itemContainer.getAllItems();
        
        return itemHashMap;
    }
    
    @Override
    public AbstractItem getItem(String key) {
        AbstractItem item = itemContainer.getItem(key);
        
        return item;
    }
    
    @Override
    public void putItem(AbstractItem item) {
        itemContainer.putItem(item);
    }
    
    @Override
    public void updateItem(AbstractItem item) {
        itemContainer.updateItem(item);
    }
    
    @Override
    public void deleteItem(AbstractItem item) {
        itemContainer.deleteItem(item);
    }
    
    @Override
    public Boolean use() {
        if (!isOpened()) {
            makeOpened();
        } else {
            makeClosed();
        }
        
        return isOpened();
    }
    
}
