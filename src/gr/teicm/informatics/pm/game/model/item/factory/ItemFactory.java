/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.item.factory;

import gr.teicm.informatics.pm.game.constant.ItemConstants;
import gr.teicm.informatics.pm.game.factory.Factory;
import gr.teicm.informatics.pm.game.model.item.*;
import gr.teicm.informatics.pm.game.model.item.consumable.*;
import gr.teicm.informatics.pm.game.model.item.miscellaneous.*;
import gr.teicm.informatics.pm.game.model.item.weapon.*;

/**
 * This class of objects can be used to create factories that created easily
 * <code>AbstractItem</code> objects.
 * 
 * @author Stamos
 */
public final class ItemFactory extends Factory<AbstractItem> {
    
    private static final String potionName =
            ItemConstants.POTION.name().toLowerCase();
    
    private static final String drugName =
            ItemConstants.DRUG.name().toLowerCase();
    
    private static final String knifeName =
            ItemConstants.KNIFE.name().toLowerCase();
    
    private static final String pistolName =
            ItemConstants.PISTOL.name().toLowerCase();
    
    private static final String bookName =
            ItemConstants.BOOK.name().toLowerCase();
    
    private static final String chestName =
            ItemConstants.CHEST.name().toLowerCase();
    
    private static final String doorName =
            ItemConstants.DOOR.name().toLowerCase();
    
    private static final String cocaineName =
            ItemConstants.COCAINE.name().toLowerCase();
    
    /**
     * Initializes a newly created <code>ItemFactory</code> object.
     */
    public ItemFactory() {
        super("Item");
        
        hashMap.put(potionName, new Potion(potionName));
        
        hashMap.put(drugName, new Drug(drugName));
        
        hashMap.put(knifeName, new Knife(knifeName));
        
        hashMap.put(pistolName, new Pistol(pistolName));
        
        hashMap.put(bookName, new Book(bookName));
        
        hashMap.put(chestName, new Chest(chestName));
        
        hashMap.put(doorName, new Door(doorName));
        
        hashMap.put(cocaineName, new Cocaine(cocaineName));
    }
    
}
