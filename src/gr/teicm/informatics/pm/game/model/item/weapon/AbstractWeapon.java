/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.item.weapon;

import gr.teicm.informatics.pm.game.model.IAttackable;
import gr.teicm.informatics.pm.game.model.item.AbstractItem;

/**
 * The superclass of weapons.
 * 
 * @author Stamos
 */
public abstract class AbstractWeapon extends AbstractItem<Integer>
implements IAttackable {
    
    /**
     * Indicates how much damage can this weapon can do.
     */
    protected Integer damageDone;
    
    /**
     * Indicates the <code>AbstractActor</code> object which is targeted.
     */
    protected String targetActorName;
    
    /**
     * Initializes a newly created <code>AbstractWeapon</code> object. Use this
     * constructor from subclasses.
     * 
     * @param name
     * @param description
     */
    protected AbstractWeapon(String name, String description) {
        super(name, description);
        
        damageDone = 0;
        
        targetActorName = "";
    }
    
    /**
     * Returns the amount of damage this weapon can do.
     * 
     * @return amount of damage done
     */
    public Integer getDamageDone() {
        return damageDone;
    }
    
    /**
     * Sets a newly value to <code>damageDone</code>.
     * 
     * @param damageDone
     */
    public void setDamageDone(Integer damageDone) {
        this.damageDone = damageDone;
    }
    
    /**
     * Returns the <code>AbstractActor</code> object's <tt>name</tt> which
     * indicates who the weapon targets at.
     * 
     * @return
     */
    public String getTargetActorName() {
        return targetActorName;
    }
    
    /**
     * Sets a new value to the <code>AbstractActor</code> object's name which
     * indicates who the weapon targets at.
     * 
     * @param targetActorName
     */
    public void setTargetActorName(String targetActorName) {
        this.targetActorName = targetActorName;
    }

    @Override
    public Integer use() {
        return attack();
    }
    
}
