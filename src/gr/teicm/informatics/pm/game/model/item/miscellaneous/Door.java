/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.item.miscellaneous;

import gr.teicm.informatics.pm.game.model.IOpening;

/**
 * The door class of the game.
 * 
 * @author Stamos
 */
public class Door extends AbstractMiscellaneous<Boolean> implements IOpening {
    
    /**
     * Indicates if the door is opened for access.
     */
    private Boolean opened;
    
    /**
     * Initializes a newly created <code>Door</code> object.
     * 
     * @param name
     */
    public Door(String name) {
        super(name, "This is a door. It can be opened or closed.");
        
        opened = false;
    }
    
    @Override
    public boolean isTakable() {
        return false;
    }
    
    @Override
    public Boolean isOpened() {
        return opened;
    }
    
    @Override
    public void makeOpened() {
        opened = true;
    }
    
    @Override
    public void makeClosed() {
        opened = false;
    }

    @Override
    public Boolean use() {
        if (!isOpened()) {
            makeOpened();
        } else {
            makeClosed();
        }
        
        return isOpened();
    }
    
}
