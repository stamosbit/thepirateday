/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model.item.weapon;

/**
 * The knife class of the game.
 * 
 * @author Stamos
 */
public class Knife extends AbstractWeapon {
    
    /**
     * Initializes a newly created <code>Knife</code> object.
     * 
     * @param name
     */
    public Knife(String name) {
        super(name, "This is a knife. You can stab someone with it.");
        
        damageDone = 5;
    }
    
    @Override
    public boolean isTakable() {
        return true;
    }
    
    @Override
    public Integer attack() {
        return damageDone;
    }
    
}
