/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.model;

/**
 *
 * @author Stamos
 */
public interface IOpenable extends java.io.Serializable {
    
    IOpening open(IOpening item);
    
    IOpening close(IOpening item);
    
}
