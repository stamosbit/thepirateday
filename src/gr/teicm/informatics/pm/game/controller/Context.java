/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.controller;

import gr.teicm.informatics.pm.game.constant.ActorConstants;
import gr.teicm.informatics.pm.game.constant.DirectionConstants;
import gr.teicm.informatics.pm.game.model.scene.container.builder.SceneContainerDirector;
import gr.teicm.informatics.pm.game.model.actor.container.IActorContainer;
import gr.teicm.informatics.pm.game.model.scene.container.ISceneContainer;
import gr.teicm.informatics.pm.game.model.actor.AbstractActor;
import gr.teicm.informatics.pm.game.model.item.AbstractItem;
import gr.teicm.informatics.pm.game.model.scene.Scene;
import gr.teicm.informatics.pm.game.retrieval.ActorContainerRetriever;
import gr.teicm.informatics.pm.game.retrieval.IRetriever;

import java.util.Iterator;
import java.util.Map.Entry;

/**
 * This class of objects can create and organize <code>GameComponent</code>
 * objects and change their status and behaviour using controllers.
 * 
 * @author Stamos
 */
public final class Context implements java.io.Serializable {
    
    private static final String PLAYER_NAME =
            ActorConstants.PLAYER.name().toLowerCase();
    
    private ISceneContainer sceneContainer;
    
    private final IActorContainer actorContainer;
    
    private final MoveActorController moveActorController;
    
    private String currentOutcome;
    
    /**
     * Initializes a newly created <tt>Context</tt> object.
     */
    public Context() {
        SceneContainerDirector sceneContainerDirector =
                new SceneContainerDirector(); 
        
        sceneContainerDirector.prepareInstance();
        
        sceneContainer = sceneContainerDirector.getInstance();
        
        IRetriever<IActorContainer> retriever =
                new ActorContainerRetriever(sceneContainer);
        
        retriever.retrieve();
        
        actorContainer = retriever.getRetrieved();
        
        moveActorController = new MoveActorController();
        
        currentOutcome = "";
    }
    
    /**
     * Returns the current game context outcome, in <tt>String</tt> form.
     * 
     * @return the current outcome of this context
     */
    public String getCurrentOutcome() {
        return currentOutcome;
    }
    
    public void resetCurrentOutcome() {
        currentOutcome = "";
    }
    
    /**
     * Sets a newly value to <tt>currentOucome</tt>.
     * 
     * @param currentOutcome
     *        the newly value for the currentOutcome
     */
    public void setCurrentOutcome(String currentOutcome) {
        this.currentOutcome = currentOutcome;
    }
    
    /**
     * Moves the player to a certain <tt>direction</tt> within the scenes.
     * 
     * @param direction
     *        the direction where the player will be moved
     */
    public void movePlayer(DirectionConstants direction) {
        moveActor(PLAYER_NAME, direction);
    }
    
    /**
     * Enables an <tt>AbstractActor</tt> object to move to the <tt>direction</tt> into
     * an <tt>ISceneContainer</tt> object.
     * 
     * @param actorName
     *        the actor's name which will be moved
     * @param direction
     *        the direction where the actor will be moved
     */
    public void moveActor(String actorName, DirectionConstants direction) {
        AbstractActor actor = actorContainer.getActor(actorName);
        
        moveActorController.setActor(actor);
        
        moveActorController.setSceneMap(sceneContainer);
        
        moveActorController.moveActor(direction);
        
        actor = moveActorController.getActor();
        
        sceneContainer = moveActorController.getSceneMap();
        
        actorContainer.updateActor(actor);
        
        moveActorController.reset();
    }
    
    /**
     * Returns game's context current status in the form of String.
     * 
     * This method should be used every time a method of the game's context is
     * used and changes the player's location.
     * 
     * @return
     */
    public String getCurrentLocation() {
        String currentLocation = "";
        
        String currentSceneName =
                actorContainer.getActor(PLAYER_NAME).getCurrentSceneName();
        
        Scene scene =
                sceneContainer.getScene(currentSceneName);
        
        String sceneDescription = scene.getDescription();
        
        currentLocation =
                "You are now in the " + currentSceneName + ".\n" +
                sceneDescription;
        
        return currentLocation;
    }
    
    /**
     * Returns the names of all <tt>AbstractItem</tt> objects placed in the
     * player's inventory.
     * 
     * @return the names of the items placed in the player's inventory
     */
    public String getPlayerInventoryStatus() {
        String inventoryStatus = "";
        
        Iterator<Entry<String,AbstractItem>> inventoryIterator =
                actorContainer.getActor(PLAYER_NAME)
                .getAllItems().entrySet().iterator();
        
        while (inventoryIterator.hasNext()) {
            AbstractItem item = inventoryIterator.next().getValue();
            
            inventoryStatus = item.getName() + "\n";
        }
        
        return inventoryStatus;
    }
    
}
