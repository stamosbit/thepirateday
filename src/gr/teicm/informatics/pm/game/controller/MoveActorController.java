/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.controller;

import gr.teicm.informatics.pm.game.constant.DirectionConstants;
import gr.teicm.informatics.pm.game.model.scene.container.ISceneContainer;
import gr.teicm.informatics.pm.game.model.actor.AbstractActor;
import gr.teicm.informatics.pm.game.model.scene.Scene;

/**
 * Objects of this class enable an <code>AbstractActor</code> object to move to
 * a direction inside an <code>ISceneContainer</code> object.
 * 
 * @author Stamos
 */
final class MoveActorController implements java.io.Serializable {
    
    private static final DirectionConstants north = DirectionConstants.NORTH;
    
    private static final DirectionConstants south = DirectionConstants.SOUTH;
    
    private static final DirectionConstants west = DirectionConstants.WEST;
    
    private static final DirectionConstants east = DirectionConstants.EAST;
    
    /**
     * The actor that is about to move.
     */
    private transient AbstractActor actor;
    
    /**
     * The collection of <code>Scene</code> objects.
     */
    private transient ISceneContainer sceneContainer;
    
    /**
     * Initializes a newly created <tt>Context</tt> object.
     */
    public MoveActorController() {
        reset();
    }
    
    /**
     * Returns the new status of the <code>AbstractActor</code> object.
     * 
     * @return the AbstractActor object
     */
    public AbstractActor getActor() {
        return actor;
    }
    
    /**
     * Sets a newly value to the <code>AbstractActor</code> property.
     * 
     * @param actor
     */
    public void setActor(AbstractActor actor) {
        this.actor = actor;
    }
    
    /**
     * Returns the new status of the <code>ISceneContainer</code> object.
     * 
     * @return
     */
    public ISceneContainer getSceneMap() {
        return sceneContainer;
    }
    
    /**
     * Sets a newly value to the <code>ISceneContainer</code> object.
     * 
     * @param sceneMap
     */
    public void setSceneMap(ISceneContainer sceneMap) {
        this.sceneContainer = sceneMap;
    }
    
    /**
     * Nullifies the basic properties.
     */
    public final void reset() {
        actor = null;
        
        sceneContainer = null;
    }
    
    /**
     * Moves the <code>AbstractActor</code> object to the <code>direction</code>
     * inside the collection of <code>Scene</code>
     * objects(<code>ISceneContainer</code> object).
     * 
     * @param direction
     */
    public void moveActor(DirectionConstants direction) {
        String currentSceneName = actor.getCurrentSceneName();
        
        Scene currentScene = sceneContainer.getScene(currentSceneName);
        
        if (north.equals(direction)) {
            String northSceneName = currentScene.getNorthSceneName();
            
            if (!(northSceneName.equals(""))) {
                currentScene.deleteActor(actor);
                
                actor.setCurrentSceneName(northSceneName);
                
                Scene northScene = sceneContainer.getScene(northSceneName);
                
                northScene.putActor(actor);
                
                sceneContainer.updateScene(currentScene);
                
                sceneContainer.updateScene(northScene);
            }
        } else if (south.equals(direction)) {
            String southSceneName = currentScene.getSouthSceneName();
            
            if (!(southSceneName.equals(""))) {
                currentScene.deleteActor(actor);
                
                actor.setCurrentSceneName(southSceneName);
                
                Scene southScene = sceneContainer.getScene(southSceneName);
                
                southScene.putActor(actor);
                
                sceneContainer.updateScene(currentScene);
                
                sceneContainer.updateScene(southScene);
            }
        } else if (west.equals(direction)) {
            String westSceneName = currentScene.getWestSceneName();
            
            if (!(westSceneName.equals(""))) {
                currentScene.deleteActor(actor);
                
                actor.setCurrentSceneName(westSceneName);
                
                Scene westScene = sceneContainer.getScene(westSceneName);
                
                westScene.putActor(actor);
                
                sceneContainer.updateScene(currentScene);
                
                sceneContainer.updateScene(westScene);
            }
        } else if (east.equals(direction)) {
            String eastSceneName = currentScene.getEastSceneName();
            
            if (!(eastSceneName.equals(""))) {
                currentScene.deleteActor(actor);
                
                actor.setCurrentSceneName(eastSceneName);
                
                Scene eastScene = sceneContainer.getScene(eastSceneName);
                
                eastScene.putActor(actor);
                
                sceneContainer.updateScene(currentScene);
                
                sceneContainer.updateScene(eastScene);
            }
        }
    }
    
}
