/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.retrieval;

import gr.teicm.informatics.pm.game.model.actor.container.IActorContainer;
import gr.teicm.informatics.pm.game.model.scene.container.ISceneContainer;
import gr.teicm.informatics.pm.game.model.actor.AbstractActor;
import gr.teicm.informatics.pm.game.model.actor.container.ActorMap;
import gr.teicm.informatics.pm.game.model.scene.Scene;

import java.util.Iterator;
import java.util.Map.Entry;

/**
 * This class of objects can be used to retrieve an <code>IActorContainer</code>
 * object collecting all <code>AbstractActor</code> objects from an
 * <code>ISceneContainer</code> object.
 * 
 * @author Stamos
 */
public final class ActorContainerRetriever implements IRetriever<IActorContainer> {
    
    private final ISceneContainer sceneContainer;
    
    private final IActorContainer actorContainer;
    
    /**
     * Initializes a newly created <tt>ActorContainerRetriever</tt> object.
     * 
     * @param sceneContainer
     */
    public ActorContainerRetriever(ISceneContainer sceneContainer) {
        this.sceneContainer = sceneContainer;
        
        actorContainer = new ActorMap();
    }
    
    /**
     * Retrieves all <tt>AbstractActor</tt> objects contained in an
     * <tt>ISceneContainer</tt> object, which is entered in the constructor, and
     * puts them in an <tt>IActorContainer</tt> object.
     */
    @Override
    public void retrieve() {
        Iterator<Entry<String,Scene>> sceneContainerIterator =
                sceneContainer.getAllScenes().entrySet().iterator();
        
        while (sceneContainerIterator.hasNext()) {
            Scene scene = sceneContainerIterator.next().getValue();
            
            Iterator<Entry<String,AbstractActor>> actorContainerIterator =
                    scene.getAllActors().entrySet().iterator();
            
            while (actorContainerIterator.hasNext()) {
                AbstractActor actor = actorContainerIterator.next().getValue();
                
                actorContainer.putActor(actor);
            }
        }
    }
    
    /**
     * Returns an <tt>IActorContainer</tt> object.
     * 
     * @return an IActorContainer object
     */
    @Override
    public IActorContainer getRetrieved() {
        return actorContainer;
    }
    
}
