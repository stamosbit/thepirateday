/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.retrieval;

/**
 * This interface can be implemented by classes which retrieve a complex object,
 * especially when creating a collection, and return it.
 * 
 * @author Stamos
 * @param <T>
 */
public interface IRetriever<T> {
    
    /**
     * Retrieves an object.
     */
    void retrieve();
    
    /**
     * Returns the retrieved object.
     * 
     * @return a retrieved object
     */
    T getRetrieved();
    
}
