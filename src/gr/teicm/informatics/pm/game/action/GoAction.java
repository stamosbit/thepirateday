/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.action;

import gr.teicm.informatics.pm.game.constant.DirectionConstants;

/**
 * This class of objects use a <code>Context</code> object to move the player.
 * 
 * @author Stamos
 */
public class GoAction extends AbstractAction {
    
    /**
     * Initializes a newly created <code>GoAction</code> object.
     * 
     * @param name
     * @param objective
     */
    public GoAction(String name, String objective) {
        super(name, objective);
    }
    
    /**
     * Moves the player into the scenes.
     */
    @Override
    public void performAction() {
        DirectionConstants direction = DirectionConstants.valueOf(objective.toUpperCase());
        
        context.movePlayer(direction);
        
        currentOutcome = context.getCurrentLocation();
        
        context.setCurrentOutcome(currentOutcome);
        
        currentOutcome = "";
    }
    
}
