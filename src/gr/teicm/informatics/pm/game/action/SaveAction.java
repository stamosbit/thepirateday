/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.action;

import gr.teicm.informatics.pm.game.serialization.ContextSerializer;
import gr.teicm.informatics.pm.game.serialization.ISerializer;

/**
 * This class of objects use an <code>ISerializer</code> object to save the
 * context.
 * 
 * @author Stamos
 */
public class SaveAction extends AbstractAction {
    
    /**
     * Initializes a newly created <code>SaveAction</code> object.
     * 
     * @param name
     * @param objective
     */
    public SaveAction(String name, String objective) {
        super(name, objective);
    }
    
    /**
     * Saves the current context of the game.
     */
    @Override
    public void performAction() {
        ISerializer serializer = new ContextSerializer(context);
        
        serializer.serialize();
    }
    
}
