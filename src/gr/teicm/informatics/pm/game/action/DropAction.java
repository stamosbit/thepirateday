/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.action;

/**
 * This class of objects use a <code>Context</code> object to delete an item
 * from the player's inventory.
 * 
 * @author Stamos
 */
public final class DropAction extends AbstractAction {
    
    /**
     * Initializes a newly created <code>DropAction</code> object.
     * 
     * @param name
     * @param objective
     */
    public DropAction(String name, String objective) {
        super(name, objective);
    }
    
    /**
     * Remove an item from the player's inventory.
     */
    @Override
    public void performAction() {
        
    }
    
}
