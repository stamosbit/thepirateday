/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.action;

import gr.teicm.informatics.pm.game.controller.Context;
import gr.teicm.informatics.pm.game.deserialization.ContextDeserializer;
import gr.teicm.informatics.pm.game.deserialization.IDeserializer;

/**
 * This class of objects use an <code>IDeserializer</code> object to load the
 * last saved context.
 * 
 * @author Stamos
 */
public class LoadAction extends AbstractAction {
    
    /**
     * Initializes a newly created <code>LoadAction</code> object.
     * 
     * @param name
     * @param objective
     */
    public LoadAction(String name, String objective) {
        super(name, objective);
    }
    
    /**
     * Loads the last saved context of the game.
     */
    @Override
    public void performAction() {
        IDeserializer<Context> deserializer = new ContextDeserializer();
        
        deserializer.deserialize();
        
        context = deserializer.getDeserialized();
    }
    
}
