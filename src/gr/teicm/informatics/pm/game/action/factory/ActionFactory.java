/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.action.factory;

import gr.teicm.informatics.pm.game.action.*;
import gr.teicm.informatics.pm.game.constant.ActionConstants;
import gr.teicm.informatics.pm.game.factory.Factory;

/**
 * This class of objects provide the ability to easily initialize
 * <code>AbstractAction</code> instances by using the
 * <code>getInstance(String type)</code> method.
 * 
 * @author Stamos
 */
public final class ActionFactory extends Factory<AbstractAction> {
    
    private static final String saveName =
            ActionConstants.SAVE.name().toLowerCase();
    
    private static final String loadName =
            ActionConstants.LOAD.name().toLowerCase();
    
    private static final String goName =
            ActionConstants.GO.name().toLowerCase();
    
    private static final String runName =
            ActionConstants.RUN.name().toLowerCase();
    
    private static final String lookName =
            ActionConstants.LOOK.name().toLowerCase();
    
    private static final String takeName =
            ActionConstants.TAKE.name().toLowerCase();
    
    private static final String dropName =
            ActionConstants.DROP.name().toLowerCase();
    
    private static final String openName =
            ActionConstants.OPEN.name().toLowerCase();
    
    private static final String closeName =
            ActionConstants.CLOSE.name().toLowerCase();
    
    private static final String shootName =
            ActionConstants.SHOOT.name().toLowerCase();
    
    private static final String noObjective = "";
    
    /**
     * Initializes a newly created <code>ActionFactory</code> object.
     * 
     * @param objective
     */
    public ActionFactory(String objective) {
        super("Action");
        
        // Put in the collection below any class' instance that extends
        // AbstractAction.
        hashMap.put(saveName, new SaveAction(saveName, noObjective));
        
        hashMap.put(loadName, new LoadAction(loadName, noObjective));
        
        hashMap.put(goName, new GoAction(goName, objective));
        
        hashMap.put(runName, new RunAction(runName, objective));
        
        hashMap.put(lookName, new LookAction(lookName, objective));
        
        hashMap.put(takeName, new TakeAction(takeName, objective));
        
        hashMap.put(dropName, new DropAction(dropName, objective));
        
        hashMap.put(openName, new OpenAction(openName, objective));
        
        hashMap.put(closeName, new CloseAction(closeName, objective));
        
        hashMap.put(shootName, new ShootAction(shootName, objective));
    }
    
}
