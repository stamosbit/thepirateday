/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.action;

import gr.teicm.informatics.pm.game.controller.Context;

/**
 * This <code>abstract</code> class is extended from any class that is needed to
 * create a different kind of action.
 * 
 * @author Stamos
 */
public abstract class AbstractAction {
    
    /**
     * The action's name.
     */
    protected final String name;
    
    /**
     * The action's parameter.
     */
    protected final String objective;
    
    /**
     * Will hold the game context's final value.
     */
    protected Context context;
    
    /**
     * Will get the context's appropiate status in string form.
     */
    protected String currentOutcome;
    
    /**
     * Initializes a newly created <code>AbstractAction</code> object. Use this
     * constructor from subclasses.
     * 
     * @param name
     * @param objective
     */
    protected AbstractAction(String name, String objective) {
        this.name = name;
        
        this.objective = objective;
        
        currentOutcome = "";
        
        context = null;
    }
    
    /**
     * Returns the value of the <code>context</code> propery.
     * 
     * @return a Context object
     */
    public Context getContext() {
        return context;
    }
    
    /**
     * Sets a newly value to <code>context</code> propery.
     * 
     * @param context
     *        the new value of context property
     */
    public void setContext(Context context) {
        this.context = context;
    }
    
    /**
     * Uses the context in a unique way.
     */
    public abstract void performAction();
    
}
