/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.action;

/**
 * This class of objects use a <code>Context</code> object to open an item.
 * 
 * @author Stamos
 */
public class OpenAction extends AbstractAction {
    
    /**
     * Initializes a newly created <code>OpenAction</code> object.
     * 
     * @param name
     * @param objective
     */
    public OpenAction(String name, String objective) {
        super(name, objective);
    }
    
    /**
     * Uses the context to open an item if possible.
     */
    @Override
    public void performAction() {
        
    }
    
}
