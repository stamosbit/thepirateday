/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.action;

/**
 * This class is an <code>AbstractAction</code> subclass and overrides the
 * <code>performAction</code> method.
 * 
 * @author Stamos
 */
public class TakeAction extends AbstractAction {
    
    /**
     * Initializes a newly created <code>TakeAction</code> object.
     * 
     * @param name
     * @param objective
     */
    public TakeAction(String name, String objective) {
        super(name, objective);
    }
    
    /**
     * Puts an item to the player's inventory.
     */
    @Override
    public void performAction() {
        
    }
    
}
