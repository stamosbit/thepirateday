/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.factory;

import java.util.HashMap;

/**
 * This class can be extended by all factories in the current projects that need
 * to function as <code>IFactory&lt;T&gt;</code>.
 * 
 * @author Stamos
 * @param <T>
 */
public class Factory<T> implements IFactory<T> {
    
    /**
     * The type of the factory.
     */
    protected final String factoryType;
    
    /**
     * Will contain all instances.
     */
    protected HashMap<String,T> hashMap;
    
    /**
     * Initializes a newly created <code>Factory</code> object. Use this
     * constructor from subclasses.
     * 
     * @param factoryType
     */
    protected Factory(String factoryType) {
        this.factoryType = factoryType;
        
        hashMap = new HashMap<>();
    }
    
    @Override
    public T getInstance(String type) throws IllegalArgumentException {
        String key = type.toLowerCase();
        
        if (hashMap.containsKey(key)) {
            T instance = hashMap.get(key);
            
            return instance;
        } else {
            IllegalArgumentException exception = new IllegalArgumentException(
                    "Invalid " + factoryType + "..."
            );
            
            throw exception;
        }
    }
    
}
