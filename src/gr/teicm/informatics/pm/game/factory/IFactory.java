/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.factory;

/**
 * This interface is implemented by the <code>Factory&lt;T&gt;</code> class
 * which supports this interface. Classes that need to implement this interface
 * can just extend the <code>Factory&lt;T&gt;</code> class.
 * 
 * @author Stamos
 * @param <T>
 */
public interface IFactory<T> {
    
    /**
     * Returns a new instance depending on the <code>type</code> argument.
     * 
     * @param type
     *        the instance type
     * @return a new instance depending on the the type
     * @throws IllegalArgumentException
     */
    T getInstance(String type) throws IllegalArgumentException;
    
}
