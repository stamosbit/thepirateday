/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.builder;

/**
 * This interface can be implemented by classes that build a container and
 * return it.
 * 
 * @author Stamos
 * @param <T>
 */
public interface IContainerBuilder<T> {
    
    void buildContainer();
    
    T getInstance();
    
}
