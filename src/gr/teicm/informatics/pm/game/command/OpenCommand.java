/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.command;

import gr.teicm.informatics.pm.game.action.AbstractAction;
import gr.teicm.informatics.pm.game.action.factory.ActionFactory;
import gr.teicm.informatics.pm.game.factory.IFactory;

/**
 * This class of objects can be used to create a command that will use the
 * context to open an item in the enviroment of the player.
 * 
 * @author Stamos
 */
public final class OpenCommand extends AbstractCommand {
    
    /**
     * Initializes a newly created <code>OpenCommand</code> object.
     * 
     * @param name
     * @param objective
     */
    public OpenCommand(String name, String objective) {
        super(name, objective);
    }
    
    @Override
    public boolean isValid() {
        return true;
    }
    
    @Override
    public void executeCommand() {
        IFactory<AbstractAction> actionFactory = new ActionFactory(objective);
        
        AbstractAction action = actionFactory.getInstance("open");
        
        game.setAction(action);
    }
    
}
