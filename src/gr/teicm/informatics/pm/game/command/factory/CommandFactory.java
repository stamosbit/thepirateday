/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.command.factory;

import gr.teicm.informatics.pm.game.command.*;
import gr.teicm.informatics.pm.game.constant.CommandConstants;
import gr.teicm.informatics.pm.game.factory.Factory;

/**
 * This class of objects provide the ability to easily initialize
 * <code>AbstractCommand</code> instances by using the
 * <code>getInstance(String type)</code> method.
 * 
 * @author Stamos
 */
public final class CommandFactory extends Factory<AbstractCommand> {
    
    private static final String exitName =
            CommandConstants.EXIT.name().toLowerCase();
    
    private static final String saveName =
            CommandConstants.SAVE.name().toLowerCase();
    
    private static final String loadName =
            CommandConstants.LOAD.name().toLowerCase();
    
    private static final String goName =
            CommandConstants.GO.name().toLowerCase();
    
    private static final String runName =
            CommandConstants.RUN.name().toLowerCase();
    
    private static final String lookName =
            CommandConstants.LOOK.name().toLowerCase();
    
    private static final String takeName =
            CommandConstants.TAKE.name().toLowerCase();
    
    private static final String dropName =
            CommandConstants.DROP.name().toLowerCase();
    
    private static final String openName =
            CommandConstants.OPEN.name().toLowerCase();
    
    private static final String closeName =
            CommandConstants.CLOSE.name().toLowerCase();
    
    private static final String shootName =
            CommandConstants.SHOOT.name().toLowerCase();
    
    private static final String noObjective = "";
    
    /**
     * Initializes a newly created <code>CommandFactory</code> object.
     * 
     * @param objective
     */
    public CommandFactory(String objective) {
        super("Command");
        
        // Put in the collection below any class' instance that extends
        // AbstractCommand.
        hashMap.put(exitName, new ExitCommand(exitName, noObjective));
        
        hashMap.put(saveName, new SaveCommand(saveName, noObjective));
        
        hashMap.put(loadName, new LoadCommand(loadName, noObjective));
        
        hashMap.put(goName, new GoCommand(goName, objective));
        
        hashMap.put(runName, new RunCommand(runName, objective));
        
        hashMap.put(lookName, new LookCommand(lookName, objective));
        
        hashMap.put(takeName, new TakeCommand(takeName, objective));
        
        hashMap.put(dropName, new DropCommand(dropName, objective));
        
        hashMap.put(openName, new OpenCommand(openName, objective));
        
        hashMap.put(closeName, new CloseCommand(closeName, objective));
        
        hashMap.put(shootName, new ShootCommand(shootName, objective));
    }
    
}
