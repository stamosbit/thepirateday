/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.command;

/**
 * This class of objects can be used to close the game.
 * 
 * @author Stamos
 */
public final class ExitCommand extends AbstractCommand {

    /**
     * Initializes a newly created <code>ExitCommand</code> object.
     * 
     * @param name
     * @param objective
     */
    public ExitCommand(String name, String objective) {
        super(name, objective);
    }
    
    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void executeCommand() {
        game.finish();
    }
    
}
