/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.command;

import gr.teicm.informatics.pm.game.action.AbstractAction;
import gr.teicm.informatics.pm.game.action.factory.ActionFactory;
import gr.teicm.informatics.pm.game.factory.IFactory;

/**
 * This class of objects can be used to create a command that will use the
 * context to close an item in the enviroment of the player.
 * 
 * @author Stamos
 */
public final class CloseCommand extends AbstractCommand {
    
    /**
     * Initializes a newly created <code>CloseCommand</code> object.
     * 
     * @param name
     * @param objective
     */
    public CloseCommand(String name, String objective) {
        super(name, objective);
    }
    
    @Override
    public boolean isValid() {
        return true;
    }
    
    @Override
    public void executeCommand() {
        IFactory<AbstractAction> actionFactory = new ActionFactory(objective);
        
        AbstractAction action = actionFactory.getInstance("close");
        
        game.setAction(action);
    }
    
}
