/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.command;

import gr.teicm.informatics.pm.game.action.AbstractAction;
import gr.teicm.informatics.pm.game.action.factory.ActionFactory;
import gr.teicm.informatics.pm.game.factory.IFactory;

/**
 * This class of objects can be used to create a command that will load the
 * context of the game.
 * 
 * @author Stamos
 */
public final class LoadCommand extends AbstractCommand {
    
    /**
     * Initializes a newly created <code>LoadCommand</code> object.
     * 
     * @param name
     * @param objective
     */
    public LoadCommand(String name, String objective) {
        super(name, objective);
    }
    
    @Override
    public boolean isValid() {
        return true;
    }
    
    @Override
    public void executeCommand() {
        IFactory<AbstractAction> actionFactory = new ActionFactory(objective);
        
        AbstractAction action = actionFactory.getInstance("load");
        
        game.setAction(action);
    }
    
}
