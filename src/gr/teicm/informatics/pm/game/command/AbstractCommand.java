/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.command;

import gr.teicm.informatics.pm.game.AbstractGame;
import gr.teicm.informatics.pm.game.constant.CommandConstants;

/**
 *
 * @author Stamos
 */
public abstract class AbstractCommand {
    
    protected static final String exitCommand =
            CommandConstants.EXIT.name().toLowerCase();
    
    protected static final String saveCommand =
            CommandConstants.SAVE.name().toLowerCase();
    
    protected static final String loadCommand =
            CommandConstants.LOAD.name().toLowerCase();
    
    protected static final String goCommand =
            CommandConstants.GO.name().toLowerCase();
    
    protected static final String runCommand =
            CommandConstants.RUN.name().toLowerCase();
    
    protected static final String lookCommand =
            CommandConstants.LOOK.name().toLowerCase();
    
    protected static final String takeCommand =
            CommandConstants.TAKE.name().toLowerCase();
    
    protected static final String dropCommand =
            CommandConstants.DROP.name().toLowerCase();
    
    protected static final String openCommand =
            CommandConstants.OPEN.name().toLowerCase();
    
    protected static final String closeCommand =
            CommandConstants.CLOSE.name().toLowerCase();
    
    protected static final String shootCommand =
            CommandConstants.SHOOT.name().toLowerCase();
    
    protected static final String noObjective = "";
    
    protected String name;
    
    protected String objective;
    
    protected AbstractGame game;
    
    /**
     * Initializes a newly created <code>AbstractCommand</code> object. Use this
     * constructor from subclasses.
     * 
     * @param name
     * @param objective
     */
    protected AbstractCommand(String name, String objective) {
        this.name = name;
        
        this.objective = objective;
        
        game = null;
    }
    
    public String getName() {
        return name;
    }
    
    public String getObjective() {
        return objective;
    }
    
    public AbstractGame getGame() {
        return game;
    }
    
    public void setGame(AbstractGame game) {
        this.game = game;
    }
    
    public abstract boolean isValid();
    
    public abstract void executeCommand();
    
}
