/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.constant;

/**
 * All key words for commands.
 * 
 * @author Stamos
 */
public enum CommandConstants {
    
    EXIT,
    
    SAVE,
    
    LOAD,
    
    GO,
    
    RUN,
    
    LOOK,
    
    TAKE,
    
    DROP,
    
    OPEN,
    
    CLOSE,
    
    SHOOT
    
}
