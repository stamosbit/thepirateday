/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.constant;

/**
 * Key words for all items.
 * 
 * @author Stamos
 */
public enum ItemConstants implements java.io.Serializable {
    
    POTION,
    
    DRUG,
    
    KNIFE,
    
    PISTOL,
    
    BOOK,
    
    CHEST,
    
    DOOR,
    
    COCAINE
    
}
