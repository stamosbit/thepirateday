/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game;

import gr.teicm.informatics.pm.game.action.AbstractAction;
import gr.teicm.informatics.pm.game.controller.Context;

/**
 * This <code>abstract</code> class is extended from any class that is needed to
 * create a different kind of game.
 * 
 * @author Stamos
 */
public abstract class AbstractGame {
    
    /**
     * Will hold any <code>Context</code> object in which actions will be
     * performed.
     */
    protected Context context;
    
    /**
     * Will hold any <code>AbstractAction</code> that will be performed.
     */
    protected AbstractAction action;
    
    /**
     * The <code>String</code> which will hold the appropiate result from any
     * performed action in the context.
     */
    protected String outcome;
    
    /**
     * Initializes a newly created <code>AbstractGame</code> object. Use this
     * constructor from subclasses.
     */
    protected AbstractGame() {
        context = new Context();
        
        action = null;
        
        outcome = "";
    }
    
    /**
     * Sets a newly value to <code>action</code> property.
     * 
     * @param action
     */
    public void setAction(AbstractAction action) {
        this.action = action;
    }
    
    /**
     * Returns the result of the game context's final status.
     * 
     * @return
     */
    public String getOutcome() {
        return outcome;
    }
    
    public void resetOutcome() {
        outcome = "";
    }
    
    /**
     * The <code>run</code> method is executed if and only if the
     * <code>action</code> property is <tt>not</tt> <code>null</code>.
     */
    public void play() {
        if (!(action == null)) {
            run();
        }
    }
    
    /**
     * Controls the game's context.
     */
    protected abstract void run();
    
    /**
     * Closes the application.
     */
    public abstract void finish();
    
}
