/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.util;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * This class of objects can be used to split a <code>String</code> object to a
 * new <code>List&lt;String&gt;</code> object.
 * 
 * @author Stamos
 */
public final class StringSplitter implements ISplitter<String> {
    
    private final StringTokenizer tokenizer;
    
    private final List<String> words;
    
    public StringSplitter(String input) {
        tokenizer = new StringTokenizer(input);
        
        words = new ArrayList<>();
    }
    
    /**
     * Splits the <code>String</code> input object to a
     * <code>List&lt;String&gt;</code> object creating a new element to the list
     * on every whitespace.
     */
    @Override
    public void split() {
        while (tokenizer.hasMoreTokens()) {
            words.add(tokenizer.nextToken());
        }
    }
    
    /**
     * Returns the split words in a <code>List&lt;String&gt;</code> object.
     * 
     * @return List of String object
     */
    @Override
    public List<String> getSplit() {
        return words;
    }
    
}
