/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.util.finder;

import java.util.List;

/**
 * This encapsulated class is to be inherited from string finder classes in this
 * package that also need to function as <code>IFinder&lt;String&gt;</code>
 * implementations.
 * 
 * @author Stamos
 */
class StringFinder implements IFinder<String> {
    
    /**
     * The target to be found from <code>words</code>.
     */
    protected String target;
    
    /**
     * The input inserted in an <code>List</code> of strings.
     */
    protected List<String> words;
    
    /**
     * Holds the values of the Enum in which target will be searched.
     */
    protected Enum[] values;
    
    /**
     * Is <tt>true</tt> when the target is found.
     */
    protected boolean targetFound;
    
    /**
     * Initializes a newly created <code>StringFinder</code> object. Use this
     * constructor from subclasses.
     * 
     * @param words
     * @param values
     */
    protected StringFinder(List<String> words, Enum[] values) {
        this.words = words;
        
        this.values = values;
        
        target = "";
        
        targetFound = false;
    }
    
    @Override
    public String getTarget() {
        return target;
    }
    
    @Override
    public boolean targetIsFound() {
        return targetFound;
    }
    
    @Override
    public void searchTarget() {
        for (Enum value : values) {
            for (String word : words) {
                if (value.name().equalsIgnoreCase(word)) {
                    target = word.toLowerCase();
                    
                    targetFound = true;
                }
            }
        }
        
        if (target.equals("")) {
            targetFound = false;
        }
    }
    
}
