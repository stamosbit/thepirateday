/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.util.finder;

/**
 * This interface is to be implemented by classes whose objects search for a
 * valid target in a collection given through a constructor and return it when
 * found.
 * 
 * @author Stamos
 * @param <T>
 */
public interface IFinder<T> {
    
    /**
     * Searches for a target in the collection given in the constructor.
     */
    void searchTarget();
    
    /**
     * Returns <tt>true</tt> when the target is found.
     * 
     * @return true when the target is found.
     */
    boolean targetIsFound();
    
    /**
     * Returns the target. It should be checked whether it was found with the
     * <code>targetIsFound()</code> method.
     * 
     * @return the target.
     */
    T getTarget();
    
}
