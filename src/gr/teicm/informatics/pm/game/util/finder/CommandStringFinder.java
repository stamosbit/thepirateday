/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.util.finder;

import gr.teicm.informatics.pm.game.constant.CommandConstants;

import java.util.List;

/**
 * The purpose for objects of this class is to search for a string matching a
 * command name into a list of words.
 * 
 * @author Stamos
 */
public final class CommandStringFinder extends StringFinder {
    
    /**
     * Initializes a newly created <code>CommandStringFinder</code> object.
     * 
     * @param words
     */
    public CommandStringFinder(List<String> words) {
        super(words, CommandConstants.values());
    }
    
}
