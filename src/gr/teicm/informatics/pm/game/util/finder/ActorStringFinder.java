/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.util.finder;

import gr.teicm.informatics.pm.game.constant.ActorConstants;

import java.util.List;

/**
 * The purpose for objects of this class is to search for a string matching an
 * actor name into a list of words.
 * 
 * @author Stamos
 */
public final class ActorStringFinder extends StringFinder {
    
    /**
     * Initializes a newly created <code>ActorStringFinder</code> object.
     * 
     * @param words
     */
    public ActorStringFinder(List<String> words) {
        super(words, ActorConstants.values());
    }
    
}
