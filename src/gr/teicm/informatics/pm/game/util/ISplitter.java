/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.util;

import java.util.List;

/**
 * This interface can be implemented by classes whose object used delimitters to
 * split objects and placing every divided element into a <code>List</code>
 * object.
 * 
 * @author Stamos
 * @param <T>
 *        the type of class whose objects will be split
 */
public interface ISplitter<T> {
    
    /**
     * Splits an object and places every element into a <code>List</code>
     * object.
     */
    void split();
    
    /**
     * Returns the <code>List</code> object.
     * 
     * @return a List
     */
    List<T> getSplit();
    
}
