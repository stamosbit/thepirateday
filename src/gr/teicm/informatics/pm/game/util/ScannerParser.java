/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.util;

import gr.teicm.informatics.pm.game.util.finder.*;
import gr.teicm.informatics.pm.game.command.AbstractCommand;
import gr.teicm.informatics.pm.game.command.factory.CommandFactory;
import gr.teicm.informatics.pm.game.factory.IFactory;

import java.util.Scanner;
import java.util.List;

/**
 * This class of objects can be used to create object that parse
 * <code>Scanner</code> objects and extract <code>AbstractCommand</code>
 * objects.
 * 
 * @author Stamos
 */
public final class ScannerParser implements IParser<Scanner,AbstractCommand> {
    
    private ISplitter<String> splitter;
    
    private List<String> splitInput;
    
    private IFinder<String> commandFinder;
    
    private IFinder<String> directionFinder;
    
    private IFinder<String> itemFinder;
    
    private IFinder<String> actorFinder;
    
    private String parsedCommand;
    
    private String parsedObjective;
    
    public ScannerParser() {
        reset();
    }
    
    @Override
    public void setInput(Scanner input) {
        splitter = new StringSplitter(input.nextLine());
    }
    
    @Override
    public void splitInput() {
        splitter.split();
        
        splitInput = splitter.getSplit();
    }
    
    @Override
    public void parse() {
        findCommand();
        
        findObjective();
    }
    
    @Override
    public AbstractCommand getParsed() {
        IFactory<AbstractCommand> commandFactory =
                new CommandFactory(parsedObjective);
                
        return commandFactory.getInstance(parsedCommand);
    }
    
    @Override
    public void reset() {
        splitter = null;
        
        splitInput = null;
        
        commandFinder = null;
        
        directionFinder = null;
        
        itemFinder = null;
        
        actorFinder = null;
        
        parsedCommand = "";
        
        parsedObjective = "";
    }
    
    private void findCommand() {
        commandFinder = new CommandStringFinder(splitInput);
        
        commandFinder.searchTarget();
        if (commandFinder.targetIsFound()) {
            parsedCommand = commandFinder.getTarget();
        }
    }
    
    private void findObjective() {
        directionFinder = new DirectionStringFinder(splitInput);
        
        itemFinder = new ItemStringFinder(splitInput);
        
        actorFinder = new ActorStringFinder(splitInput);
        
        switch (parsedCommand) {
            case "go":
                directionFinder.searchTarget();
                if (directionFinder.targetIsFound()) {
                    parsedObjective = directionFinder.getTarget();
                }
                break;
            
            case "take":
                itemFinder.searchTarget();
                if (itemFinder.targetIsFound()) {
                    parsedObjective = itemFinder.getTarget();
                }
                break;
            
            case "run":
                directionFinder.searchTarget();
                if (directionFinder.targetIsFound()) {
                    parsedObjective = directionFinder.getTarget();
                }
                break;
            
            case "open":
                itemFinder.searchTarget();
                if (itemFinder.targetIsFound()) {
                    parsedObjective = itemFinder.getTarget();
                }
                break;
            
            case "close":
                itemFinder.searchTarget();
                if (itemFinder.targetIsFound()) {
                    parsedObjective = itemFinder.getTarget();
                }
                break;
            
            case "shoot":
                actorFinder.searchTarget();
                if (actorFinder.targetIsFound()) {
                    parsedObjective = actorFinder.getTarget();
                }
                break;
            
            default:
                parsedObjective = "";
                break;
        }
    }
    
}
