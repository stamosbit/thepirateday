/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teicm.informatics.pm.game.util;

/**
 * This interface can be implemented by classes that need to create objects
 * which will function as parsers of input and extract another type of objects.
 * 
 * @author Stamos
 * @param <T>
 *        the input type of class whose objects will be parsed
 * @param <E>
 *        the type of class whose objects will be got when parsed
 */
public interface IParser<T,E> {
    
    /**
     * Sets a newly calue to <code>input</code> to be parsed.
     * 
     * @param input
     */
    void setInput(T input);
    
    /**
     * Splits the <code>input</code> object using an <code>ISplitter</code>
     * object.
     */
    void splitInput();
    
    /**
     * Analyzes the <code>List</code> taken from the <code>ISplitter</code>
     * object.
     */
    void parse();
    
    /**
     * Extracts an object using factory.
     * 
     * @return
     */
    E getParsed();
    
    /**
     * Resets all basic properties to their initial value.
     */
    void reset();
    
}
